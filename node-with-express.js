const express = require('express');

const app = express();

app.get('/', (req, res) => {
	res.send('My response with Express!');
});

app.listen(3000);
